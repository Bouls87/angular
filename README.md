# Sandbox

`ng new sandbox`

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

# Directive 

Une directive est un élément du framework qui va directement intéragir avec le DOM de la page :  
* Directive d'attributs ==> modifie l'élément html sur lequel elles sont associées ==> [ngClass] ==> Son role est d'appliquer ou non une classe CSS.
* Directives structurelles ==> Modifier élement HTML sur lequel elles sont ajoutées ==> *ngFor 
* Directives composants

# Les décorateurs 

Permettent de dire au framework comment interpréter un élément ==> @Component ==> Sans le décorateur @Component, la classe aurait été considérée comme une classe TypeScript simple

Exemple de décorateurs : 
@Input -- @Inject -- @Injectable -- @ViewChild

# Les pipes 

Gères les transformations de valeurs ==> Exemple pour les dates. S'écrit directement dans le template html {{today | date:"dd/MM/yyyy"}}

On peut définir au niveau du module la locale par défaut. Cela se fait en utilisant la fonction **registerLocaleData** 

{{today | date:"short"}}
{{today | date:"long"}}
...


Les pipes: 
* *async* prend en entrée une promesse ou observable et en affiche le résultat.
* *uppercase* et *lowercase*
* *json* ==> Affiche objet au format json
* *decimal*
* *percent*
* *currency*
* *i18nPlural* ==> Une sorte de switch en fonction de l'entrée

Création de pipes : 
* Classe qui implémente l'interface PipeTransform
  * Contient une seule méthode : transform(value: any, ...args: any[]) : any;

@Pipe({name: "PIPE_NAME"})
export class GreetingPipe implements PipeTransform {

  transform(value: string, isMale: boolean) : string {
    return "";
  }

}
  * Et enfin pour qu'il puisse être utilisé, il faut l'ajouter au décorateur @Component dans l'attribut pipes

NB : On peut se servir des pipes pour appliquer des filtres en tout genre

* Pipe pur : Ne se rafraichit que lorsqu'il y a un changement de référence. L'ajout d'un nouvel objet dans un tableau par exemple ne réactualisera pas le pipe
* Pipe impur : Moins performant ==> Se réactualise à chaque fois dans @Pipe { pure: false}

# Callback Promise et Observables 

JS est un langage par nature fortement asynchrone. L'asynchronisme est géré via un système de callback. 

Exemple callback : 
myAsynchronousMethod(function(data) {
  console.log(data);
});

Le callback passé en paramètre est éxécuté lorsque l'appel asynchrone est terminé

Exemple Promise ( = callback de succès ET d'erreur):
myAsynchronousMethod.then(function(data) {
  console.log(data);
  },function(error) {
  console.error(error);
});


Exemple Obervable RxJS( = A la différence d'une Promise : il peut être résolu plusieurs fois afin de renvoyer plusieurs valeurs):
myAsynchronousMethod.subscribe(function(data) {
console.log(data);
},function(error) {
console.error(error);
});
