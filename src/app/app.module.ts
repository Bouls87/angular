import {LOCALE_ID, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import {FormsModule} from "@angular/forms";
import {registerLocaleData} from "@angular/common";
import localeFr from "@angular/common/locales/fr"

registerLocaleData(localeFr, "fr-FR")

@NgModule({
  // TODO : Défini tous les éléments embarqués dans le module : composants, directives, pipes
  declarations: [
    AppComponent,
    TodoListComponent
  ],
  // TODO : Importe des modules afin de pouvoir afin de pouvoir utiliser tous les éléments de ces modules dans le module courant
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [{provide: LOCALE_ID, useValue: "fr-FR"}], // TODO défini les éléments qui seront injectable dans tous les composants du module
  bootstrap: [AppComponent]  // TODO Element sur lequel l'application Angular va démarrer
})
export class AppModule { }
