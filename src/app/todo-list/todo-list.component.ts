import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.sass']
})
export class TodoListComponent implements OnInit {

  monInput: string = "";
  list: string[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  create() {
    if (this.monInput) {
      this.list.push(this.monInput);
      this.monInput = '';
    }
  }

  delete() {
    this.list = [];
  }

}
